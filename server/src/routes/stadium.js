const express = require('express');
const { Stadium } = require('../models/stadium');

const router = express.Router();

router.get('/', (req, res) => {
    Promise.all([
        Stadium.find({})
            .limit(req.body.perPage)
            .skip(req.body.page)
            .exec(),
        Stadium.count({}),
    ])
        .then(([stadiums, total]) => res.json({ data: stadiums, total }))
        .catch((error) => res.status(400).json({ error: error.message }));
});

router.get('/:id', (req, res) => {
    const { id } = req.params;
    Stadium.findOne({ id })
        .then((stadium) => {
            if (stadium) res.json(stadium);
            else res.status(404).json({ error: `Stadium not found with id: ${id}` });
        })
        .catch((error) => res.status(400).json({ error: error.message }));
});

router.post('/:id/reset', async (req, res) => {
    const { id } = req.params;
    Stadium.findOne({ id })
        .then((stadium) => {
            const updatedStadium = stadium;
            updatedStadium.boost_percentage = stadium.default_boost_percentage;
            Stadium.findByIdAndUpdate(updatedStadium._id, { $set: updatedStadium },
                { new: true, runValidators: true })
                .then((newStadium) => res.status(200).json(newStadium))
                .catch((error) => res.status(400).json({ error }));
        });
});

module.exports = router;
