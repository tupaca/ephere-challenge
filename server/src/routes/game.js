const express = require('express');
const { Stadium } = require('../models/stadium');

const router = express.Router();

// eslint-disable-next-line max-len
const getScore = (boost = 0) => Math.floor(Math.sqrt(Math.floor((Math.random() * (1 + (boost / 100)) * 10), 2)));

router.post('/', async (req, res) => {
    const { stadiumId } = req.body;
    Stadium.findOne({ id: stadiumId })
        .then((stadium) => {
            const homeScore = getScore(stadium?.boost_percentage);
            const awayScore = getScore();

            if (stadium) {
                const updatedStadium = stadium;
                if (updatedStadium.boost_percentage > 0) updatedStadium.boost_percentage -= 1;
                updatedStadium.games_played += 1;
                if (homeScore > awayScore) {
                    updatedStadium.games_won_as_local += 1;
                }

                Stadium.findByIdAndUpdate(updatedStadium._id, { $set: updatedStadium },
                    { new: true, runValidators: true })
                    .then(() => res.json({ homeScore, awayScore }))
                    .catch((error) => {
                        console.error('error 400', error);
                        res.status(400).json({ error: error.message });
                    });
            } else res.json({ homeScore, awayScore });
        });
});

module.exports = router;
