const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose
    .connect(process.env.MONGODB_URI)
    .then(() => console.info('🚀 Mongo connected'))
    .catch((err) => console.error(err));

mongoose.set('toJSON', {
    virtuals: true,
    transform: (doc, converted) => {
        // eslint-disable-next-line no-param-reassign
        delete converted._id;
        // eslint-disable-next-line no-param-reassign, no-underscore-dangle
        delete converted.__v;
    },
});
