const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const server = express();
server.use(cors());

server.use(morgan('dev'));
server.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
server.use(bodyParser.json({ limit: '50mb' }));

// Routes
server.use('/static', express.static('public'));
server.use('/stadiums', require('./routes/stadium'));
server.use('/games', require('./routes/game'));

module.exports = server;
