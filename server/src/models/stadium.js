const mongoose = require('mongoose');

const StadiumSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        trim: true,
        required: true,
    },
    image: {
        type: String,
        trim: true,
        required: true,
    },
    boost_percentage: {
        type: Number,
        required: true,
    },
    default_boost_percentage: {
        type: Number,
        required: true,
    },
    games_played: {
        type: Number,
        required: true,
    },
    games_won_as_local: {
        type: Number,
        required: true,
    },
}, { collection: 'stadiums' });

StadiumSchema.virtual('attributes').get(function getAttributes() {
    return [{
        display_type: 'boost_percentage',
        trait_type: 'Actual team performance enhancement',
        value: this.boost_percentage,
    }, {
        display_type: 'default_boost_percentage',
        trait_type: 'Default team performance enhancement',
        value: this.default_boost_percentage,
    }, {
        display_type: 'number',
        trait_type: 'Games played',
        value: this.games_played,
    }, {
        display_type: 'number',
        trait_type: 'Games won as local',
        value: this.games_won_as_local,
    }];
});

module.exports = {
    Stadium: mongoose.model('Stadium', StadiumSchema),
};
