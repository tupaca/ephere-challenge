require('dotenv').config({ path: `${__dirname}/./../../.env` });
require('../mongoose');
const { Stadium } = require('../models/stadium');

const stadiums = require('./data/stadiums.json');

Promise.all(stadiums.map((stadium) => Stadium.create(stadium)))
    .then(() => {
        console.info('Done seeding stadiums');
        process.exit();
    })
    .catch((err) => {
        console.error(err);
        process.exit();
    });
