require('dotenv').config();
require('./mongoose');
const https = require('https');
const fs = require('fs');
const http = require('http');
const server = require('./server');

if (process.env.HTTPS_SELFSIGNED === 'true') {
    const sslOptions = {
        key: fs.readFileSync('sslcert/localhost.key'),
        cert: fs.readFileSync('sslcert/localhost.crt'),
    };
    https.createServer(sslOptions, server).listen(process.env.PORT);
    console.info(`🚀 Server listening on port ${process.env.PORT} (HTTPS)`);
} else {
    http.createServer(server).listen(process.env.PORT);
    console.info(`🚀 Server listening on port ${process.env.PORT}`);
}
