var EphereFootballerERC721 = artifacts.require("./EphereFootballerERC721.sol");
var EphereStadiumERC721 = artifacts.require("./EphereStadiumERC721.sol");

const mintFootballers = async () => {
    const ephereFootballerInstance = await EphereFootballerERC721.deployed();
    const cids = [
        "QmeFLsNqR1f45vkfLf2ZdyjngCnNKEKcTq9qkVyt7Bj8in",
        "QmbQPwSbU9rYMdNP292oKvug9xyfYWrPoPXGPsX4TXdUzP",
        "QmaYqNSovcNSqapq9WhgGfouhcn8aDyPjuXAHWSrEZown8",
        "QmPjAZiAsTEJ2B9V1Q7a7VnJMoZcUDwoutxbDmjaa2aD61",
        "QmRDjuV3bmdMoNGnX85MpKon1G8snRkpAUnVqQeHxsutxg",
        "QmXedZDfX8XCGKWS8vvLk1JmwkAFFG86P6VRCP6pLasrMo",
        "QmWrrkKSKEW56yqKSe2BZEbinjRUP6nqNiysBavm8CQLfy",
        "QmRrokZArTs2cBmUvbgBS8Hwi15DWAF11iqREUk9w3fk2W",
        "QmPakS8d12hPtvuWW4SgQvA91n79jjeE8xJ4WWMpUZ5uj7",
        "QmYXQjXj4XE7jwadvLqKJM7hWApuuhQGdLp1SrkEyDrcRg",
        "QmUqxfbUDXpeh2KVN7ZTmxGgQUs4EFwnjYd2G3ZW2b8ShC",
        "QmdRXQypmcvqabX9ctfcPZjQ3gzPXTv2B9wspvQ2Dm9Wrv",
        "QmStMxadvMs4rfbhwCmsDkr55H3vw6qPbdmXh2yL8Fs1XU",
        "QmWYkQtRan4GhroBaRByPCJMDtd8HLFN5gg8eNCK4Lac7p",
        "QmWP7pWyzHqAYwnhCZiDo1o5K35QxuhqLuGiGutVctMF1n",
        "QmaU9P3rCQ5cdevmufwRCzWk41Sp23bPur9GMkiPS3s6N4",
        "QmV3yhE1KZtno7zv8fSWFEWYScEaCvcgcRr8PRuiPSSJse",
        "QmVwkCuoenJJdanAQxKNET6c3rgxAxJUXpbuaMieEuJEFu",
        "QmbbREFXUTmgmuttmYBF6XPotUVbwzgBEzDPyg6MUWC9Ah",
        "QmbEwJDgM6rrGTUNv3eWv8K1owmXyXs6WKzPCqYDRN6LGa",
    ];

    for(const cid of cids) {
        try {
            await ephereFootballerInstance.mint(cid, process.env.NFT_OWNER_ADDRESS);
        } catch (e) {
            console.error(e);
        }
    }

    console.log("Minting footballers complete");
};

const mintStadiums = async () => {
    const ephereStadiumInstance = await EphereStadiumERC721.deployed();
    const ids = [1, 2, 3, 4];
    for(const id of ids) {
        try {
            await ephereStadiumInstance.safeMint(process.env.NFT_OWNER_ADDRESS, id);
        } catch (e) {
            console.error(e);
        }
    }

    console.log("Minting Stadiums complete");
};

module.exports = async (done) => {
    await mintFootballers();
    await mintStadiums();
    done();
};
