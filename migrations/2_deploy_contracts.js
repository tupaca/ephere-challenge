var EphereFootballerERC721 = artifacts.require("./EphereFootballerERC721.sol");
var EphereStadiumERC721 = artifacts.require("./EphereStadiumERC721.sol");

module.exports = async function (deployer) {
  deployer.deploy(EphereFootballerERC721, 50);
  deployer.deploy(EphereStadiumERC721);
};

