##### **Proyecto boilerplate de Truffle + React + MUI**

El ejemplo tiene un unico smart contract de prueba.

Es necesario configurar los .env antes de empezar a trabajar. 
Dejamos un ejemplo en cada uno:
- /.env (template en /.env.template)
- /server/.env (template en /server/.env.template)
- /client/.env (template en /client/.env.template)

###### **Comandos para empezar:**
- Abrir ganache
- `yarn` (instala dependencias)
- `cd server`
- `yarn` (instala dependencias)
- `yarn createStadiums` (crea estadios en la DB)
- `cd ..`
- `truffle deploy` (deploya los contratos de prueba)
- `truffle exec scripts/mint.js` (mintea asignando como owner a la account del .env)
- `cd client`
- `yarn` (instala dependencias)
- `yarn start` (corre la dapp)


_Truffle al deployar los contratos, los agrega al proyecto de react en /client._


**TODO:** 
- Botón de conexión con metamask en vez de conectar a la fuerza
- Parametrizar distintas redes en truffle (mainnet, ropsten, rinkeby, etc). Hoy solamente se usa local.
