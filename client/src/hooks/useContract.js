/* eslint-disable import/prefer-default-export */
import { useEffect, useState } from 'react';
import { useWeb3React } from '@web3-react/core';

export const useContract = (contractData) => {
    const {
        active, account, library,
    } = useWeb3React();
    const [contract, setContract] = useState(null);

    useEffect(() => {
        if (!active) return;
        // Get the contract instance.
        library.eth.net.getId().then((networkId) => {
            const deployedNetwork = contractData.networks[networkId];
            setContract(new library.eth.Contract(
                contractData.abi, deployedNetwork && deployedNetwork.address,
            ));
        }).catch((err) => {
            // alert('Failed to load network id.');
            console.error(err);
        });
    }, [contractData.abi, contractData.networks, library, active]);

    return {
        library, account, contract, contractName: contractData.contractName,
    };
};
