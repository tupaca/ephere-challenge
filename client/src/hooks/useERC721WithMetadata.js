/* eslint-disable import/prefer-default-export */
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useContract } from './useContract';

export const useERC721WithMetadata = (contractData) => {
    const [loading, setLoading] = useState(true);
    const { account, contract, contractName } = useContract(contractData);
    const [ownedTokens, setOwnedTokens] = useState([]);
    const [quantity, setQuantity] = useState(0);

    useEffect(() => {
        if (!contract) return;
        contract.methods.balanceOf(account)
            .call()
            .then((balance) => {
                setQuantity(balance);
            });
    }, [account, contract]);

    const refreshTokenMetadata = (tokenId) => {
        const token = ownedTokens.find((t) => t.id === tokenId);
        axios
            .get(token.uri)
            .then((response) => {
                const index = ownedTokens.findIndex((t) => t.id === tokenId);
                const newToken = {
                    ...ownedTokens[index],
                    metadata: response.data,
                };
                setOwnedTokens([
                    ...ownedTokens.slice(0, index),
                    newToken,
                    ...ownedTokens.slice(index + 1),
                ]);
            })
            .catch((error) => console.error(error));
    };

    useEffect(() => {
        if (!contract || !quantity) return;
        const tokenPromises = [];
        for (let i = 0; i < quantity; i += 1) {
            const tokenIdPromise = contract.methods.tokenOfOwnerByIndex(account, i).call();
            const URIPromise = tokenIdPromise
                .then((tokenId) => contract.methods.tokenURI(tokenId).call());
            const metadataPromise = URIPromise
                .then((uri) => axios.get(uri).then((response) => response.data));
            tokenPromises.push(Promise.all([tokenIdPromise, URIPromise, metadataPromise]));
        }
        Promise
            .all(tokenPromises)
            .then((tokensData) => {
                const tokens = tokensData.map(([id, uri, metadata]) => ({ id, uri, metadata }));
                setOwnedTokens(tokens);
            })
            .catch((error) => {
                console.error(error);
            })
            .finally(() => setLoading(false));
    }, [contractName, contract, account, quantity]);
    return {
        quantity, ownedTokens, loading, refreshTokenMetadata,
    };
};
