import { createTheme } from '@mui/material';
import BenzinBold from './fonts/Benzin Bold.ttf';

export default createTheme({
    typography: {
        fontFamily: "'Benzin-Bold', sans-serif",
    },
    components: {
        MuiCssBaseline: {
            styleOverrides: `
                @font-face {
                  font-family: 'Benzin-Bold';
                  font-style: normal;
                  font-display: swap;
                  font-weight: 700;
                  src: url(${BenzinBold}) format('true-type');
                }
              `,
        },
        MuiButton: {
            styleOverrides: {
                contained: {
                    borderRadius: '50px',
                },
                outlined: {
                    borderRadius: '50px',
                },
            },
        },
        MuiInputBase: {
            styleOverrides: {
                root: {
                    backgroundColor: '#2E0082',
                },
            },
        },
        MuiOutlinedInput: {
            styleOverrides: {
                root: {
                    borderRadius: '50px',
                },
            },
        },
        MuiCard: {
            styleOverrides: {
                root: {
                    backgroundColor: '#2E0082',
                    borderRadius: 0,
                },
            },
        },
    },
    palette: {
        text: {
            primary: '#ffffff',
        },
        background: {
            default: '#5C12E2',
        },
        primary: {
            main: '#EA31E2',
            light: '#EA31E2',
        },
    },
});
