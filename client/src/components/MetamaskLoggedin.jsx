import React, { useEffect } from 'react';
import { useWeb3React } from '@web3-react/core';
import { Box, Button, Container } from '@mui/material';
import Connector from './Connector';
import NextMatchScreen from './screens/NextMatchScreen';

const MetamaskLoggedin = ({ children }) => {
    const {
        active, account, activate, error,
    } = useWeb3React();

    const connect = async () => {
        try {
            await activate(Connector);
        } catch (ex) {
            console.error(ex);
        }
    };

    useEffect(() => {
        Connector
            .isAuthorized()
            .then((isAuthorized) => {
                if (isAuthorized && !active && !error) {
                    activate(Connector);
                }
                if (error) {
                    console.error(error);
                }
            }).catch((ex) => {
                console.error(ex);
            });
    }, [activate, active, error]);

    if (active && account) return children;
    return (
        <Container>
            <Box mt={2} display="flex" justifyContent="center">
                <Button variant="contained" onClick={connect}>Connect to MetaMask</Button>
            </Box>
        </Container>
    );
};

export default MetamaskLoggedin;
