import React from 'react';
import { Box } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => createStyles({
    container: {
        backgroundColor: '#7628C3',
    },
    active: {
        backgroundColor: '#EA31E2',
        color: '#FFFFFF',
        fontWeight: 700,
        fontSize: 40,
        textTransform: 'uppercase',
    },
    notActive: {
        color: '#FFFFFF',
        fontWeight: 700,
        fontSize: 40,
        textTransform: 'uppercase',
    },
}));

const Header = () => {
    const classes = useStyles();

    return (
        <Box className={classes.container} display="flex">
            <Box className={classes.active} py={1} px={5}>NEXT MATCH</Box>
        </Box>
    );
};

export default Header;
