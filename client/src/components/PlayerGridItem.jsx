import React, { useEffect, useState } from 'react';
import { makeStyles, createStyles } from '@mui/styles';
import {
    Box,
    ButtonBase,
    Card,
    CardActionArea,
    CardContent,
    CardMedia, Grid,
    Typography,
} from '@mui/material';

const useStyles = makeStyles(() => createStyles({
    unchecked: {
        borderWidth: 3,
        borderStyle: 'solid',
        borderColor: 'transparent',
        '&:hover': {
            borderColor: '#EA31E2',
        },
    },
    checked: {
        borderWidth: 3,
        borderStyle: 'solid',
        borderColor: '#EA31E2',
        position: 'relative',
        filter: 'drop-shadow(4px 4px 6px rgba(0, 0, 0, 0.15))',
    },

    image: {
        height: 128,
        width: 128,
        objectFit: 'cover',
    },
    content: {
        padding: 0,
    },
    name: {
        backgroundColor: '#230062',
        padding: 5,
        fontSize: 20,
        textTransform: 'uppercase',
        lineClamp: 1,
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
    },
}));

const PlayerGridItem = ({
    isSelected, image, name, changeSelectionStatus, ...rest
}) => {
    const classes = useStyles();

    const handleChange = () => {
        changeSelectionStatus(!isSelected);
    };

    return (
        <Grid item {...rest}>
            <Card className={`${isSelected ? classes.checked : classes.unchecked}`}>
                <CardActionArea onClick={handleChange}>
                    <Box className={classes.name}>
                        <Typography
                            fontWeight={700}
                            whiteSpace="nowrap"
                            textOverflow="ellipsis"
                            overflow="hidden"
                        >
                            {name}
                        </Typography>
                    </Box>
                    <CardMedia
                        className={classes.image}
                        component="img"
                        height="240"
                        image={image}
                        alt={name}
                    />
                </CardActionArea>
            </Card>
        </Grid>
    );
};

export default PlayerGridItem;
