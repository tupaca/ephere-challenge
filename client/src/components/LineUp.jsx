import React, { useState } from 'react';
import { Box, Typography } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => createStyles({
    container: {
        backgroundColor: '#2E0082',
        borderTopColor: '#1BEABB',
        borderTopWidth: 20,
        borderTopStyle: 'solid',
        width: 340,
        maxWidth: '100%',
    },
    footballer: {
        padding: '0px 20px',
        textTransform: 'uppercase',
        fontSize: 26,
    },
    footballersList: {
        '&>:nth-child(odd)': {
            backgroundColor: '#230062',
        },
    },
}));

const LineUp = ({ selectedFootballers }) => {
    const classes = useStyles();

    return (
        <Box py={1} className={classes.container} flexShrink={0}>
            <Typography fontWeight={700} maxWidth={100} fontSize={30} lineHeight={1} p={2}>
                STARTING LINEUP
            </Typography>
            <Box className={classes.footballersList}>
                {selectedFootballers.map((footballer, index) => (
                    <Box key={footballer.id} className={classes.footballer} display="flex" py={1} px={2}>
                        <Typography width={30} fontWeight={700}>
                            {index + 1}
                        </Typography>
                        <Typography fontWeight={700}>
                            {footballer.metadata.name}
                        </Typography>
                    </Box>
                ))}
            </Box>
        </Box>
    );
};

export default LineUp;
