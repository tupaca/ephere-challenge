import React from 'react';
import {
    Box, Button, Grid, Skeleton,
} from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import { useERC721WithMetadata } from '../../hooks/useERC721WithMetadata';
import EphereFootballerERC721Data from '../../contracts/EphereFootballerERC721.json';
import PlayerGridItem from '../PlayerGridItem';
import StepHeader from './StepHeader';

const useStyles = makeStyles(() => createStyles({
    buttonNext: {
        '&:disabled': {
            backgroundColor: '#e0e0e0 !important',
            color: '#bdbdbd !important',
        },
    },
}));

const ChooseEphereals = ({
    goNextStep, selectedFootballers, setSelectedFootballers,
}) => {
    const classes = useStyles();
    const { loading, ownedTokens } = useERC721WithMetadata(EphereFootballerERC721Data);

    const setFootballerSelectionStatus = (footballer) => (isSelected) => {
        if (isSelected && selectedFootballers.length < 11) {
            setSelectedFootballers([...selectedFootballers, footballer]);
        } else setSelectedFootballers(selectedFootballers.filter((f) => f.id !== footballer.id));
    };

    const isSelected = (footballer) => selectedFootballers.some((f) => f.id === footballer.id);

    const gridItemProps = { xs: 6, sm: 3, lg: 2 };

    const skeletons = Array(11).fill(0).map((value, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Grid key={index} item {...gridItemProps}>
            <Skeleton variant="rectangular" width="100%" height={200} />
        </Grid>
    ));

    return (
        <Box>
            <StepHeader
                title="CHOOSE YOUR EPHEREALS"
                counter={`${selectedFootballers.length} / 11`}
                button={(
                    <Button
                        variant="contained"
                        disabled={selectedFootballers.length < 11}
                        onClick={() => goNextStep()}
                        className={classes.buttonNext}
                    >
                        CONTINUE
                    </Button>
                )}
            />
            <Grid container spacing={2}>
                { loading
                    ? skeletons
                    : ownedTokens.map((footballer) => (
                        <PlayerGridItem
                            key={footballer.id}
                            image={footballer.metadata.image}
                            name={footballer.metadata.name}
                            changeSelectionStatus={setFootballerSelectionStatus(footballer)}
                            isSelected={isSelected(footballer)}
                            {...gridItemProps}
                        />
                    ))}
            </Grid>
        </Box>
    );
};

export default ChooseEphereals;
