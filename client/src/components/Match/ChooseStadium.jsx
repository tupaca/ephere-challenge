import React from 'react';
import {
    Box, Button, Grid, Skeleton, Typography,
} from '@mui/material';
import axios from 'axios';
import { useERC721WithMetadata } from '../../hooks/useERC721WithMetadata';
import EphereStadiumERC721Data from '../../contracts/EphereStadiumERC721.json';
import StepHeader from './StepHeader';
import StadiumGridItem from '../StadiumGridItem';

const ChooseStadium = ({
    goNextStep, goPreviousStep, setSelectedStadium, selectedStadium,
}) => {
    const {
        loading, ownedTokens, refreshTokenMetadata,
    } = useERC721WithMetadata(EphereStadiumERC721Data);

    const gridItemProps = { xs: 12, sm: 12, lg: 6 };

    const skeletons = Array(8).fill(0).map((value, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Grid key={index} item {...gridItemProps}>
            <Skeleton variant="rectangular" width="100%" height={270} />
        </Grid>
    ));

    const resetStadium = (stadiumId) => {
        axios.post(`${process.env.REACT_APP_API_URL}/stadiums/${stadiumId}/reset`)
            .then(() => refreshTokenMetadata(stadiumId))
            .catch((error) => console.error(error));
    };

    return (
        <Box>
            <StepHeader
                title="CHOOSE YOUR STADIUM"
                button={(
                    <Box display="flex">
                        <Box mx={1}>
                            <Button variant="outlined" onClick={() => goPreviousStep()}>
                                <Typography noWrap>GO BACK</Typography>
                            </Button>
                        </Box>
                        <Box mx={1}>
                            <Button variant="contained" onClick={() => goNextStep()}>CONTINUE</Button>
                        </Box>
                    </Box>
                )}
            />
            <Grid container spacing={2}>
                { loading
                    ? skeletons
                    : ownedTokens.map((stadium) => (
                        <StadiumGridItem
                            key={stadium.id}
                            id={stadium.id}
                            name={stadium.metadata.name}
                            image={stadium.metadata.image}
                            performanceBenefit={stadium.metadata.boost_percentage}
                            changeSelectionStatus={() => {
                                setSelectedStadium(
                                    selectedStadium?.id === stadium.id
                                        ? null
                                        : stadium,
                                );
                            }}
                            isSelected={selectedStadium?.id === stadium.id}
                            resetStadium={resetStadium}
                            {...gridItemProps}
                        />
                    ))}
            </Grid>
        </Box>
    );
};

export default ChooseStadium;
