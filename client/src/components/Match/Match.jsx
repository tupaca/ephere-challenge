import React, { useEffect, useState } from 'react';
import { Box } from '@mui/material';
import axios from 'axios';
import ChooseEphereals from './ChooseEphereals';
import ChooseStadium from './ChooseStadium';
import ChooseRival from './ChooseRival';
import Result from './Result';

const Match = ({
    selectedFootballers, setSelectedFootballers, setMatchFinished, setMatchResult,
}) => {
    const [actualStep, setActualStep] = useState(0);
    const [selectedStadium, setSelectedStadium] = useState(null);
    const [rivalAddress, setRivalAddress] = useState('');

    const goNextStep = () => {
        setActualStep(actualStep + 1);
    };

    const goPreviousStep = () => {
        setActualStep(actualStep - 1);
    };

    const playMatch = () => {
        axios.post(`${process.env.REACT_APP_API_URL}/games`, {
            footballers: selectedFootballers.map((footballer) => footballer.id),
            stadiumId: selectedStadium?.id,
            rivalAccount: rivalAddress,
        }).then((response) => {
            setMatchResult(response.data);
            setMatchFinished(true);
        }).catch((error) => {
            console.error(error);
        });
    };

    const steps = [
        <ChooseEphereals
            goNextStep={goNextStep}
            selectedFootballers={selectedFootballers}
            setSelectedFootballers={setSelectedFootballers}
        />,
        <ChooseStadium
            goNextStep={goNextStep}
            goPreviousStep={goPreviousStep}
            selectedStadium={selectedStadium}
            setSelectedStadium={setSelectedStadium}
        />,
        <ChooseRival
            goPreviousStep={goPreviousStep}
            goNextStep={goNextStep}
            rivalAddress={rivalAddress}
            setRivalAddress={setRivalAddress}
            playMatch={playMatch}
        />,
    ];

    return (
        <Box mx={2} mt={2} flexGrow={1}>
            {steps[actualStep]}
        </Box>
    );
};

export default Match;
