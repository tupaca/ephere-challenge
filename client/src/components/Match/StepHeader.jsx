import { Box, Typography } from '@mui/material';
import React from 'react';
import { createStyles, makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => createStyles({
    outlineStroke: {
        '-webkit-text-stroke': '1px white',
        '-webkit-text-fill-color': 'transparent',
    },
}));

const StepHeader = ({ title, counter = null, button }) => {
    const classes = useStyles();
    return (
        <Box display="flex" alignItems="center" justifyContent="space-between">
            <Typography
                fontSize={36}
                fontWeight={700}
                className={classes.outlineStroke}
            >
                {title}
            </Typography>
            {counter && (
                <Typography fontWeight={700} fontSize={36} noWrap flexShrink={0} mx={1}>
                    {counter}
                </Typography>
            )}
            {button}
        </Box>
    );
};

export default StepHeader;
