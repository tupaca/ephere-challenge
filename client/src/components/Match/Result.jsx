import React, { useEffect, useState } from 'react';
import {
    Box, Button, Container, Typography,
} from '@mui/material';
import axios from 'axios';
import { createStyles, makeStyles } from '@mui/styles';
import StepHeader from './StepHeader';
import logoHome from '../../home.svg';
import logoAway from '../../away.svg';

const useStyles = makeStyles(() => createStyles({
    result: {
        display: 'block',
    },
}));

const Result = ({
    goPreviousStep, selectedFootballers, selectedStadium, rivalAddress,
}) => {
    const classes = useStyles();
    const [homeScore, setHomeScore] = useState('');
    const [awayScore, setAwayScore] = useState('');

    useEffect(() => {
        axios.post(`${process.env.REACT_APP_API_URL}/games`, {
            footballers: selectedFootballers.map((footballer) => footballer.id),
            stadiumId: selectedStadium?.id,
            rivalAccount: rivalAddress,
        }).then((response) => {
            setHomeScore(response.data.homeScore);
            setAwayScore(response.data.awayScore);
        }).catch((error) => {
            console.error(error);
        });
    }, [selectedFootballers, selectedStadium, rivalAddress]);

    const handleContinue = () => {
        window.location.href = '/';
    };

    return (
        <Box>
            <StepHeader
                title="RESULT"
                button={(
                    <Box>
                        <Button variant="contained" onClick={() => goPreviousStep()}>GO BACK</Button>
                    </Box>
                )}
            />
            <Box display="flex" alignItems="center" justifyContent="space-between">
                <img src={logoHome} alt="home" />
                <Box display="flex" flexDirection="column">
                    <Box display="flex" alignItems="center">
                        <Typography variant="h1">
                            {homeScore}
                            {' '}
                            {awayScore}
                        </Typography>
                    </Box>
                    <Button variant="contained" onClick={handleContinue}>NEW MATCH</Button>
                </Box>
                <img src={logoAway} alt="away" />
            </Box>
        </Box>
    );
};

export default Result;
