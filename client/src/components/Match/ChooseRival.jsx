import React from 'react';
import {
    Box,
    Button,
    FormControl,
    InputAdornment,
    OutlinedInput, Typography,
} from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import StepHeader from './StepHeader';

const useStyles = makeStyles(() => createStyles({
    playButton: {
        '&:disabled': {
            backgroundColor: '#e0e0e0 !important',
            color: '#bdbdbd !important',
        },
    },
}));
const ChooseRival = ({
    goPreviousStep, playMatch, rivalAddress, setRivalAddress,
}) => {
    const [playing, setPlaying] = React.useState(false);
    const classes = useStyles();

    const handleChange = (e) => {
        if (e.target.value.length <= 42) setRivalAddress(e.target.value);
    };

    const PlayButton = () => (
        <Button
            className={classes.playButton}
            variant="contained"
            onClick={() => { setPlaying(true); playMatch(); }}
            disabled={playing || rivalAddress?.length !== 42}
        >
            PLAY
        </Button>
    );

    return (
        <Box>
            <StepHeader
                title="CHOOSE YOUR RIVAL"
                button={(
                    <Box display="flex">
                        <Box mx={1}>
                            <Button variant="outlined" onClick={() => goPreviousStep()}>
                                <Typography noWrap>GO BACK</Typography>
                            </Button>
                        </Box>
                    </Box>
                )}
            />
            <FormControl sx={{ my: 1, width: 500, maxWidth: 'calc(100% - 50px)' }} variant="outlined">
                <OutlinedInput
                    type="text"
                    value={rivalAddress}
                    onChange={handleChange}
                    placeholder="0x00000..."
                    endAdornment={<InputAdornment position="end"><PlayButton /></InputAdornment>}
                />
            </FormControl>

        </Box>
    );
};

export default ChooseRival;
