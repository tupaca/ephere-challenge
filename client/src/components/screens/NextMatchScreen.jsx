import React, { useState } from 'react';
import { Box } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import Match from '../Match/Match';
import LineUp from '../LineUp';

const useStyles = makeStyles(() => createStyles({
    container: {
        height: 'calc(100vh - 90px)',
        width: '100%',
    },
}));

const NextMatchScreen = ({ setMatchFinished, setMatchResult }) => {
    const [selectedFootballers, setSelectedFootballers] = useState([]);

    const classes = useStyles();
    return (
        <Box className={classes.container} mt={2} display="flex">
            <Box sx={{ display: { xs: 'none', md: 'block' } }}>
                <LineUp selectedFootballers={selectedFootballers} />
            </Box>
            <Match
                setSelectedFootballers={setSelectedFootballers}
                selectedFootballers={selectedFootballers}
                setMatchFinished={setMatchFinished}
                setMatchResult={setMatchResult}
            />
        </Box>
    );
};

export default NextMatchScreen;
