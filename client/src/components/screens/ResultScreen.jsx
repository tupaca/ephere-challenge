import React, { useEffect, useState } from 'react';
import {
    Box, Button, Container, Typography,
} from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import axios from 'axios';
import StepHeader from '../Match/StepHeader';
import logoHome from '../../home.svg';
import logoAway from '../../away.svg';

const useStyles = makeStyles(() => createStyles({
    result: {
        display: 'block',
    },
    outlineStroke: {
        '-webkit-text-stroke': '1px white',
        '-webkit-text-fill-color': 'transparent',
    },
    image: {
        maxWidth: '100%',
    },
}));

const ResultScreen = ({
    goPreviousStep, reset, matchResult,
}) => {
    const classes = useStyles();

    const getTitle = () => {
        if (matchResult.homeScore > matchResult.awayScore) return 'YOU WON';
        if (matchResult.homeScore < matchResult.awayScore) return 'YOU LOST';
        return 'DRAW';
    };

    return (
        <Container>
            <Typography
                textAlign="center"
                fontSize={50}
                fontWeight={700}
                className={classes.outlineStroke}
            >
                {getTitle()}
            </Typography>
            <Box display="flex" alignItems="center" justifyContent="space-between">
                <Box flexShrink={1}>
                    <img className={classes.image} src={logoHome} alt="home" />
                </Box>
                <Box display="flex" flexDirection="column">
                    <Box display="flex" alignItems="center">
                        <Typography variant="h1" noWrap>
                            {matchResult.homeScore}
                            {' - '}
                            {matchResult.awayScore}
                        </Typography>
                    </Box>
                    <Button variant="contained" onClick={reset}>NEW MATCH</Button>
                </Box>
                <Box flexShrink={1}>
                    <img className={classes.image} src={logoAway} alt="away" />
                </Box>
            </Box>
        </Container>
    );
};

export default ResultScreen;
