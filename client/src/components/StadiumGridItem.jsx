import React, { useEffect, useState } from 'react';
import { makeStyles, createStyles } from '@mui/styles';
import {
    Box, Button,
    ButtonBase,
    Card,
    CardActionArea,
    CardContent,
    CardMedia, Grid, IconButton,
    Typography,
} from '@mui/material';
import axios from 'axios';
import { RestartAltRounded } from '@mui/icons-material';

const useStyles = makeStyles(() => createStyles({
    performance: {
        textShadow: '0 0 5px #000',
    },
    unchecked: {
        borderWidth: 3,
        borderStyle: 'solid',
        borderColor: 'transparent',
        '&:hover': {
            borderColor: '#EA31E2',
        },
    },
    checked: {
        borderWidth: 3,
        borderStyle: 'solid',
        borderColor: '#EA31E2',
        position: 'relative',
        filter: 'drop-shadow(4px 4px 6px rgba(0, 0, 0, 0.15))',
    },

    image: {
        height: 264,
        objectFit: 'cover',
    },
    content: {
        padding: 0,
    },
    name: {
        fontSize: 20,
        textTransform: 'uppercase',
        lineClamp: 1,
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
    },
    title: {
        backgroundColor: '#230062',
    },
}));

const StadiumGridItem = ({
    id, isSelected, image, name, changeSelectionStatus, performanceBenefit, resetStadium, ...rest
}) => {
    const classes = useStyles();

    const handleChange = () => {
        changeSelectionStatus(!isSelected);
    };

    return (
        <Grid item {...rest} position="relative">
            <Card className={`${isSelected ? classes.checked : classes.unchecked}`}>
                <Box p={1} className={classes.title} display="flex" justifyContent="space-between">
                    <Typography
                        className={classes.name}
                        fontWeight={700}
                        whiteSpace="nowrap"
                        textOverflow="ellipsis"
                        overflow="hidden"
                    >
                        {name}
                    </Typography>
                    <Button
                        variant="outlined"
                        onClick={() => { resetStadium(id); }}
                        endIcon={<RestartAltRounded />}
                    >
                        Reset
                    </Button>
                </Box>
                <CardActionArea onClick={handleChange}>
                    <CardMedia
                        className={classes.image}
                        component="img"
                        image={image}
                        alt={name}
                    />
                    <Box position="absolute" bottom={0} p={1}>
                        <Typography fontWeight={700} fontSize={23} className={classes.performance}>
                            +
                            {performanceBenefit}
                            %
                            {' '}
                            team performance improvement
                        </Typography>
                    </Box>
                </CardActionArea>
            </Card>
        </Grid>
    );
};

export default StadiumGridItem;
