import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@emotion/react';
import { Web3ReactProvider } from '@web3-react/core';
import Web3 from 'web3';
import Header from './components/Header';
import theme from './theme';
import MetamaskLoggedin from './components/MetamaskLoggedin';
import NextMatchScreen from './components/screens/NextMatchScreen';
import ResultScreen from './components/screens/ResultScreen';

const App = () => {
    const getLibrary = (provider) => new Web3(provider);
    const [matchFinished, setMatchFinished] = React.useState(false);
    const [matchResult, setMatchResult] = React.useState(false);

    const reset = () => { setMatchResult(null); setMatchFinished(false); };
    return (
        <Web3ReactProvider getLibrary={getLibrary}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Header />
                <MetamaskLoggedin>
                    {matchFinished
                        ? (
                            <ResultScreen
                                reset={reset}
                                matchResult={matchResult}
                            />
                        )
                        : (
                            <NextMatchScreen
                                setMatchFinished={setMatchFinished}
                                setMatchResult={setMatchResult}
                            />
                        )}
                </MetamaskLoggedin>
            </ThemeProvider>
        </Web3ReactProvider>
    );
};

export default App;
